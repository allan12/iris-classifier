import iris.utils as ui 

categories = ['engineers','press','googlers']

columns = ["screen_name","description"]

path = 'experts_audited1'

for category in categories:

	dataset = ui.loadXLSX('../audit/users_'+category+'yes.xlsx')
	dataset = dataset.parse("People")[columns]

	dataset['description'] = dataset['description'].map(lambda com : ui.clean(com))
	# print(dataset.head(5))


	for index, row in dataset.iterrows():
		fileName = row['screen_name']
		fileContent = row['description']

		pathToSave = path+'/'+category.capitalize()+'/'+fileName+'.txt'

		print(pathToSave)
		
		ui.saveTxt(pathToSave, fileContent)
