import iris.utils as ui 
import os
import sys

# model = 'output/files2'
# outputFolder = 'prediction-nn-excluded'
# resultsfile = 'results-nn.csv'

if len(sys.argv) == 4:

    categories = ['engineers','press','googlers']
    values = ['yes','no']

    model = sys.argv[1]
    outputFolder = sys.argv[2]

    results = open(sys.argv[3],'w')

    for category in categories:

        results.write(category.capitalize()+"\n")

        total = 0
        right = 0 

        for value in values:

            fileName = category+value+'.csv'
            filePath = 'audition/users_'+fileName
            
            outputPath = outputFolder+'/'+fileName


            os.system('python iris-nn-classifier.py '+filePath+' '+model+' '+outputPath)


            prediction = ui.loadCSV(outputPath)

            different = prediction[prediction["Prediction"] != category.capitalize()]["Prediction"].count()
            equal = prediction[prediction["Prediction"] == category.capitalize()]["Prediction"].count()

            total += different + equal
            right += equal

            results.write("\tValue: "+value.capitalize()+"\n")
            results.write("\t\tDifferent: "+str(different)+"\n")
            results.write("\t\tEqual: "+str(equal)+"\n")

        results.write("Accuracy: "+str(right/total)+"\n")       

    results.close()

else:
    print("python automator.py <model> <outputfolder> <resultsfile>")
    print("e.g.: python automator.py  output/files2 prediction-nn-excluded results-nn-excluded.csv")