import iris.utils as ui 

root = 'experts2'
ui.createFolder(root)

files = ['z-bulk-bio.csv','z-bulk-nobio.csv']

for file in files:

	f = open(file, 'r', encoding='utf-8')


	for line in f:
		lista = line.strip().split(';')

		if len(lista[2]) > 1:

			if lista[1] == 'Students':

				path = ui.joinFolder(root, lista[1])
				ui.createFolder(path)

				file_user = ui.joinFile(path, lista[0])

				ui.createFile(file_user, lista[2])

				print(file_user)

	f.close()