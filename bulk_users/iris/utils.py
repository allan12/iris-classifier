import os
from functools import reduce
import pandas as pd 
import re
import string
from nltk.corpus import stopwords

experts_folder = 'experts_withnone'
experts_excluded_folder = 'experts_excluded'
itself = 'wxyz'

categories = {
    "Sports": ["Sports","Sports And Fitness","Sport","Sports & Fitness Enthusiasts","Sports, Health & Fitness Enthusiasts",'Sports And Fitness'],
    "Tech": ["Technology","Tech Professionals","Tech Enthusiast","Technologist","Tech Enthusiasts","Tech Enthusiasts","Tech Enthusiasts","Tech"],
    "Politics": ['Politics',"News/Politics Enthusiast","News/Politics Enthusiast"],
    "Entertainment": ["Entertainment", "Entertainment","Entertainment Enthusiasts"],
    "Finance & Marketing": ["Finance &  Business","Finance & _Business","Finance & Marketing","Marketing, Finance & Business Professionals", "Marketing, Finance & Business","Marketing, Finance & Business",'Marketing, Finance & Business'],
    "Executives": ['Executives'],
    "Gov't Officials & Policy Maker": ["Gov'T Officials & Policy Maker", "Gov'T Officials & Policy Makers"],
    "Googlers": ["Googlers"],
    "Organizations": ["Organizations"],
    "Students": ["Students"],
    "Press": ["Press"],
    "Apple": ["Apple Employees","Apple Fanboys"],
    "Engineers": ["Engineers"],
    "Entrepreneurs & Small Biz Owners": ["Entrepreneurs & Small Biz Owners"],
    "Educators": ["Educators"],
    "Art": ["Arts & Culture Champs","Art And Culture","Arts & Design Practioners", "Arts & Culture Champs","Arts & Culture Champs"],
    "News": ["News Enthusiasts",],
    "Travel": ["Leisure, Travel, Lifestyle","Travel Enthusiasts"],
    "Game": ["Gamers"],
    "Content Creators": ["Content Creators"],
    "Fanboy": ["Fanboy","Fan Boys","Fan Boy","Fanboys",'Fan Boys','Fan Boy'],
    "Science": ["Sciences","Applied  Sciences","Applied Sciences","Applied _Sciences"]
}

stopwords = stopwords.words('english') + stopwords.words('spanish') + ['help','hi','like'] 

def loadAll(rootdir):

    dir = {}
    rootdir = rootdir.rstrip(os.sep)
    start = rootdir.rfind(os.sep) + 1
    for path, dirs, files in os.walk(rootdir):
        folders = path[start:].split(os.sep)
        subdir = dict.fromkeys(files)
        parent = reduce(dict.get, folders[:-1], dir)
        parent[folders[-1]] = subdir
    return dir



def createFile(name, content):
    if not os.path.isfile(name):
        print(name)
        file = open(name, "w", encoding="utf-8")
        file.write(content)
        file.close()

def removeFile(name):
    if os.path.isfile(name):
        os.remove(name)

def createFolder(path):

    if not os.path.exists(path):
        os.makedirs(path)

def joinFolder(path, subpath):
    return '\\'.join([path, subpath])

def joinFile(path, file, extension = ".txt"):
    return '\\'.join([path, file+extension])


def cleanBio(bio):
    bio = bio.strip()
    #delete urls
    bio = re.sub(r'^https?:\/\/.*[\r\n]*', '', bio, flags=re.MULTILINE)
    bio = re.sub(r"http\S+", "", bio, flags=re.MULTILINE)
    #delete emails
    bio = re.sub(r'[\w\.-]+@[\w\.-]+',"", bio, flags=re.MULTILINE)
    #delete punctuation except hashtags and mentions to users
    exclude = set(string.punctuation.replace('/',',').replace('#','').replace('@',''))
    exclude.add('°')
    exclude.add('”')
    exclude.add('“')
    bio = ''.join(c for c in bio if c not in exclude)
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)
    bio = emoji_pattern.sub(r'', bio) # no emoji
    return bio

def removeStopWords(text):
    word_list = text.split(' ')
    filtered_words = [word for word in word_list if word not in stopwords]
    return ' '.join(filtered_words)

def clean(text):
    text = cleanBio(str(text).lower())
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "can not ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r"\'scuse", " excuse ", text)
    text = re.sub('\W', ' ', text)
    text = re.sub('\s+', ' ', text)
    text = text.strip('')
    withoutSW = removeStopWords(text)
    withouNumbers = ' '.join([ text for text in withoutSW.split(' ') if not text.isdigit()])
    return withouNumbers


def loadCSV(pathFile):
    return pd.read_csv(pathFile, encoding = "ISO-8859-1")

def saveCSV(pathFile, dataset, sep=','):
    dataset.to_csv(pathFile, sep=sep, index=False)


def loadXLSX(pathFile):
    return pd.ExcelFile(pathFile)


def saveTxt(pathFile, content):
    f = open(pathFile,"w",encoding="utf-8")
    f.write(content)
    f.close()

def readCSV(pathFile):
    return [ line.strip() for line in open(pathFile,"r",encoding="utf-8").readlines() ]