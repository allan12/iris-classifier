import pandas as pd
import iris.utils as ui 


categories = ui.categories

def getCategory(column_name):
	column = column_name.replace('_',' ').replace('-',' ').replace('\n',' ').strip().title()
	for k,v in categories.items():
		if column in v:
			return k,True
	return column_name,False	


bio = pd.ExcelFile('BulkUsers-BiO.xlsx')

fbulk = open('z-bulk-bio.csv','w+', encoding='utf-8')

ctg = dict()

for sheet in bio.sheet_names:

	df = bio.parse(sheet)

	df = df[df['description'].notnull()]

	# print(sheet, df.size, df.columns.values)

	

	for idx, row in df.iterrows():
		screen_name = row[0]
		description = ui.clean(row[1])
		onlycolumns = row[2:].to_frame().reset_index()
		columns_names = list(onlycolumns.iloc[:,0][(onlycolumns==1).iloc[:,1]])

		# print(screen_name, description, columns_names )#onlycolumns.columns[(onlycolumns!=0)[]] )

		for column_name in columns_names:

			column_name, tf = getCategory(column_name)

			values = [str(screen_name), column_name, str(description) ]
			if len(values) == 3 and tf:
				text = ';'.join(values)
				print(text)
				fbulk.write(text+'\n')

				ctg[column_name] = ctg.get(column_name, 0) + 1

fbulk.close()

for k,v in ctg.items():
	print(k,v)


# Engineers 300
# Entrepreneurs & Small Biz Owners 488
# Politics 1249
# Tech 2474
# Press 399
# Entertainment 2887
# Sports 1457
# Content Creators 611
# Finance & Marketing 936
# Educators 184
# Students 446
# Game 368
# Science 318
# Executives 210
# Gov't Officials & Policy Maker 374
# Googlers 86
# Fanboy 124
# Art 174
# News 224
# Travel 27