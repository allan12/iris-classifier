import pandas as pd
import iris.utils as ui 

categories = ui.categories

def getCategory(column_name):
	column = column_name.replace('_',' ').replace('-',' ').replace('\n',' ').strip().title()
	for k,v in categories.items():
		if column in v:
			return k,True
	return '',False	



noBio = pd.ExcelFile('BulkUsers-noBiO.xlsx')

bios = pd.ExcelFile('bio_BulkUser-noBio.xlsx')
dfBios = bios.parse(bios.sheet_names[0])
dfBios = dfBios[['screen_name','description']]
dfBios = dfBios[dfBios['description'].notnull()]

fbulk = open('z-bulk-nobio.csv','w+', encoding='utf-8')
fnousers = open('no-users.csv','w+', encoding='utf-8')

ctg = dict()

for sheet in noBio.sheet_names:

	if 'Users' != sheet:

		df = noBio.parse(sheet)
		# print(sheet, df.size, df.columns.values)

		screen_name = df.columns.values[0]
		onlycolumns = df.columns.values[1:]
		
		names = df[screen_name]
		for name in names:

			if len(str(name).strip()) > 0:

				results = dfBios[dfBios['screen_name']==name]
				
				if results.size > 0:
					onlyvalues = df[df['screen_name'] == name][onlycolumns]
					columns_names = list(onlyvalues.columns[(onlyvalues==1.0).iloc[0]])
					description = ui.clean(results['description'].values[0])

					for column_name in columns_names:

						column_name, tf = getCategory(column_name)

						values = [name, column_name, description ]
						if len(values) == 3 and tf:
							text = ';'.join(values)
							print(text)
							fbulk.write(text+'\n')

							ctg[column_name] = ctg.get(column_name, 0) + 1
				else:
					fnousers.write(str(name)+'\n')


fbulk.close()
fnousers.close()

for k,v in ctg.items():
	print(k,v)

# Content Creators 929
# Engineers 352
# Apple 26
# Tech 3314
# Entrepreneurs & Small Biz Owners 446
# Entertainment 2533
# Sports 959
# Organizations 183
# News 1017
# Press 81
# Art 555
# Finance & Marketing 292
# Executives 30
# Students 471
# Travel 248
# Gov't Officials & Policy Maker 104
# Fanboy 828
# Googlers 259
# Politics 491
# Educators 167
# Science 111
# Game 4


# 		columns += list(df.columns.values)

# for column in columns:
# 	category, value = getCategory(column)
# 	if value:
# 		print(category, column)
