import iris.utils as ui

import pymongo
from pymongo import MongoClient
from bson.code import Code
import os
import threading


def createProfile(folderName, categories, user, bio):

    ui.createFolder(folderName)

    for category in categories: 
        
        subpath = ui.joinFolder(folderName,category)
        ui.createFolder(subpath)

        subpath = ui.joinFile(subpath,user)
        ui.createFile(subpath, bio)


client = MongoClient('mongodb://allan:iris@ds149324.mlab.com:49324/iris')
users = client.iris.bio.distinct("name")


filtered = False

for user in users:
    
    decisions = client.iris.bio.find({"name":user})
    
    categories = set()

    for decision in decisions:

        categories.add(decision["category"])
        bio = decision["bio"]

    categories = list(categories)


    if filtered:
        if 'None' in categories and len(categories) != 1:
            categories.remove('None')

    
    folderName = ui.experts_excluded_folder if filtered else ui.experts_folder

    t = threading.Thread(target=createProfile, args=(folderName, categories, user, bio))
    t.start()

print("Created all categorized users")