import iris.utils as ui 


files = ui.loadAll('test')


for root, files in files.items():
	for file in files:

		if file.endswith('.xlsx'):
			archive = ui.loadXLSX('test/'+file)
			archive = archive.parse(file.replace('.xlsx',''))

			archive = archive[['Author ID','Bio']]

			ui.saveCSV('test/'+file.replace('.csv.xlsx','')+'-1.csv', archive)