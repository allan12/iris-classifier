import iris.utils as ui 


experts = ui.loadAll('experts')

pC, pF = dict(), dict()
total = 0
categories = []


for k,v in experts.items():
	for k1,v1 in experts[k].items():

		pC[k1] = len(experts[k][k1].keys())
		total += len(experts[k][k1].keys())

		for k2, v2 in experts[k][k1].items():
			lst = pF.get(k2,[])
			lst.append(k1)
			pF[k2] = lst

		categories.append(k1)
		
for k,v in pC.items():
	print(k,v,round(v*100/total,2))


categories = list(set(categories))
print("CrossOver")

pCross = dict()

for category in categories:

	pCrossI = pCross.get( category ,dict())

	for k,v in pF.items():

		if category in v and len(v) > 1:

			for cI in v:
				
				if category != cI:
					pCrossI[cI] = pCrossI.get(cI,0) + 1 

					# if category == "Engineers":
					# 	print(cI," with ",k)

	pCross[category] = pCrossI


for k,v in pCross.items():
	total = 0
	
	for k1, v1 in v.items():
		total+=v1
	
	print(k,total, v)

