import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree

	
from  sklearn.utils import resample

import iris.utils as ui 
from os import listdir
from os.path import isfile, join
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import TweetTokenizer
tknzr = TweetTokenizer()

def loadExperts(mypath):
	words = []
	docs = []
	categories = []

	onlyfolders = [f for f in listdir(mypath) if join(mypath, f)]
	for folder in onlyfolders:
		onlyfiles = [f for f in listdir(mypath+"/"+folder) if isfile(join(mypath+"/"+folder, f))]
		categories.append(folder)

		for filename in onlyfiles:

			alist = [' '.join(tknzr.tokenize(ui.clean(line.rstrip().lower()))) for line in open(mypath+"/"+folder+'/'+filename,"r", encoding='utf-8')]
			alist = [word for word in ' '.join(alist).split(" ") if word not in ui.stopwords]
			docs.append( (alist, folder) )

			# if '008shehad' in filename:
			# 	print( alist, folder )

			words += alist
	
	words = list(set( [word for word in list(words) if not(word.isdigit()) and len(word) > 1 ] ))
	words.sort()


	return docs,categories, words

def getSet(docs, categories, words):
	stemmer = LancasterStemmer()
	# create our training data
	training = []
	output = []
	
	for doc in docs:
	    # initialize our bag of words(bow) for each document in the list
	    bow = []
	    # list of tokenized words for the pattern
	    token_words = doc[0]
	    # stem each word
	    token_words = [stemmer.stem(word.lower()) for word in token_words]
	    # create our bag of words array
	    for w in words:
	        bow.append(1) if w in token_words else bow.append(0)

	    # our training set will contain a the bag of words model and the output row that tells
	    # which catefory that bow belongs to.
	    training.append( [doc[1]]+bow )

	return training

print('Loading experts ... ')
docs, categories, words = it.loadExperts('experts2')


print('Creating matrix ... ')
mySet = it.getSet(docs, categories, words)
balance_data = pd.DataFrame(mySet, columns=["iris-category"]+words)

# total values on each category
print(balance_data['iris-category'].value_counts())

print(balance_data.head(10))

#Upsampling
indexes = balance_data['iris-category'].value_counts().index.tolist()
counts = balance_data['iris-category'].value_counts().tolist()

majority = max(counts)
avg = int(sum(counts)/len(counts))

# maxCategory = indexes[counts.index(max(counts))]

# balance_data_majority = balance_data[balance_data['iris-category']==maxCategory]
# balance_data_upsampled = pd.concat([balance_data_majority])

fits = []
fits.append(avg)
for cnt in counts:
	if cnt > avg:
		fits.append(cnt)

print("TO FIT: ", fits)
print("Categories: ",categories)

for fit in fits:

	print("Current fit: ", fit)

	balance_data_upsampled = pd.DataFrame() 

	for category in categories:

		balance_data_minority = balance_data[balance_data['iris-category']==category]

		balance_data_minority = resample(balance_data_minority, 
	                                 replace=True,     # sample with replacement
	                                 n_samples=fit,    # to match majority class
	                                 random_state=123) # reproducible results

		balance_data_upsampled = pd.concat([balance_data_upsampled, balance_data_minority])

	# for category in categories:
	# 	# if category != maxCategory:

	# 	balance_data_minority = balance_data[balance_data['iris-category']==category]

	# 	balance_data_minority_upsampled = resample(balance_data_minority, 
 #                                 replace=True,     # sample with replacement
 #                                 n_samples=fit,    # to match majority class
 #                                 random_state=123) # reproducible results

	# 	balance_data_upsampled = pd.concat([balance_data_upsampled, balance_data_minority_upsampled])

	# print(balance_data_upsampled.head(10))
	print(balance_data_upsampled['iris-category'].value_counts())

	X = balance_data.values[:, 1:]
	Y = balance_data.values[:,0]

	X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.3, random_state = 100)

	clf_gini = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5)
	clf_gini.fit(X_train, y_train)

	y_pred = clf_gini.predict(X_test)
	print("Accuracy is ", accuracy_score(y_test,y_pred)*100)


	clf_entropy = DecisionTreeClassifier(criterion = "entropy", random_state = 100, max_depth=3, min_samples_leaf=5)
	clf_entropy.fit(X_train, y_train)

	y_pred = clf_entropy.predict(X_test)
	print("Accuracy is ", accuracy_score(y_test,y_pred)*100)
