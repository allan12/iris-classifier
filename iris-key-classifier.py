import sys

def find_similar(tfidf_matrix, index, top_n = 5):
    cosine_similarities = linear_kernel(tfidf_matrix[index:index+1], tfidf_matrix).flatten()
    related_docs_indices = [i for i in cosine_similarities.argsort()[::-1] if i != index]
    return [(index, cosine_similarities[index]) for index in related_docs_indices][0:top_n]

def createCorpus():
	categories = []
	corpus = []

	folderRoot = 'experts'
	files = ui.loadAll(folderRoot)

	for category, values in files[folderRoot].items():
		
		categories.append(category)

		filePath = '\\'.join([folderRoot, category, '/*.txt'])
		for file in glob.glob(filePath):
			with open(file, "r", encoding='utf-8') as paper:
				corpus.append((file, ui.removeStopWords(ui.clean(paper.read()))) ) 

	return corpus, categories

def createMatrix(corpus):
	tf = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df = 0, stop_words = 'english')
	tfidf_matrix =  tf.fit_transform([content for file, content in corpus])

	return tfidf_matrix


def loadTest(filePath, corpus):

	with open(filePath, "r", encoding='utf-8') as paper:
		for line in paper.readlines():
			elements = line.strip().split(",")
			if len(elements) == 2:
				corpus.append((ui.itself+'+'+elements[0], ui.removeStopWords(ui.clean( elements[1]))) ) 


def prediction(output_file):

	indexesToPredict = [index for index in range(len(corpus)) if ui.itself in corpus[index][0]]

	predicted = []

	for idx in indexesToPredict:

		results = dict()

		prediction = []

		wordsNew = set(corpus[idx][1].split(' '))

		x,user = corpus[idx][0].split("+")
		text = corpus[idx][1]

		total, nitems = 0,0

		for index, score in find_similar(tfidf_matrix, idx, top_n=10):

			if not ui.itself in corpus[index][0]:

				wordRelated = set(corpus[index][1].split(' '))
				for k in categories:
					if k in corpus[index][0]:
						v = results.get(k, dict())
						v['counter'] = v.get('counter',0) + 1
						v['cumsum'] = v.get('cumsum',0) + round(score,2)

						total += round(score,2)
						nitems += 1

						results[k] = v
					
		newlabels = []
		for k,v in results.items():
			category = k
			factor = round(round(100*round(v['cumsum'],2)*round(v['counter'],2),2)/round(1+(round(total,2)*round(nitems,2))),2)
			newlabels.append((category, factor))

		newlabels.sort(key=lambda tup: tup[1], reverse=True)
		newlabels = [str(newlabel[0]) for newlabel in newlabels]

		prediction.append(user)
		prediction.append(';'.join(newlabels))
		prediction.append(text)
		predicted.append(prediction)


	file = open(output_file,'w',encoding='utf-8')
	for pre in predicted:
		text = ','.join(pre)
		file.write(text+'\n')
	file.close()

if len(sys.argv) == 3:

	import glob
	import iris.utils as ui
	from sklearn.metrics.pairwise import linear_kernel
	from sklearn.feature_extraction.text import TfidfVectorizer

	input_test = sys.argv[1]
	output_file = sys.argv[2]

	print('Creating Corpus...')
	corpus, categories = createCorpus()

	print('Loading files to classify...')
	loadTest(input_test, corpus)

	print('Creating Matrix...')
	tfidf_matrix = createMatrix(corpus)

	print('Classifying...')
	prediction(output_file)