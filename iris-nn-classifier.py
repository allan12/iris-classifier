import sys

def clean(bio):
    bio = bio.strip()
    #delete urls
    bio = re.sub(r'^https?:\/\/.*[\r\n]*', '', bio, flags=re.MULTILINE)
    bio = re.sub(r"http\S+", "", bio, flags=re.MULTILINE)
    #delete emails
    bio = re.sub(r'[\w\.-]+@[\w\.-]+',"", bio, flags=re.MULTILINE)
    #delete punctuation except mentions and users
    exclude = set(string.punctuation.replace('#','').replace('@',''))
    exclude.add('°')
    exclude.add('”')
    exclude.add('“')
    bio = ''.join(c for c in bio if c not in exclude)
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)
    bio = emoji_pattern.sub(r'', bio) # no emoji
    return bio

def getRoot(defaultOutput):
    return [ defaultOutput+"\\"+file for file in ['model.tflearn',"dimension.csv","categories.csv","words.csv"] ]

def loadCategories(categories):
    return [line.rstrip() for line in open(categories, "r+",encoding="utf-8")]

def loadWords(words):
    return [line.rstrip() for line in open(words, "r+",encoding="utf-8")]

def readDimensions(dimension):
    values = [line.rstrip() for line in open(dimension, "r+",encoding="utf-8")]
    return values[0].split(',')
    

def full_prediction(user,sentence,categories, model,words):
    record = get_tf_record(sentence,words)
    predict = model.predict([record])

    value = str(round(np.max(predict),2))
    category = categories[np.argmax(predict)].title()

    values = ','.join([ str(round(value,2)) for value in predict.round(decimals=2).tolist()[0] ])
    
    # nwords = np.array(words)
    # print(user, "record: ", nwords[record==1], category, value)

    return (user,sentence,category, value,values)

def prediction(user,sentence,categories, model,words):
    record = get_tf_record(sentence,words)
    nwords = np.array(words)
    predict = model.predict([record])
    value = np.max(predict)
    category = categories[np.argmax(predict)].title()

    # print("record: ", nwords[record==1], category, value)

    return (user,sentence,category, value)

# a method that takes in a sentence and list of all words
# and returns the data in a form the can be fed to tensorflow

def get_tf_record(sentence,words):
    # initialize the stemmer
    stemmer = LancasterStemmer()
    # tokenize the pattern
    #sentence_words = nltk.word_tokenize(sentence)

    tknzr = TweetTokenizer()
    sentence_words = [word for word in tknzr.tokenize(clean(sentence.rstrip().lower())) if word not in stopwords.words('english')]
    #print("after tokenizer: ",sentence_words)

    # stem each word
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    # bag of words
    bow = [0]*len(words)
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                bow[i] = 1

    return(np.array(bow))

if len(sys.argv) == 4:

    import nltk
    from nltk.stem.lancaster import LancasterStemmer
    from nltk.corpus import stopwords
    import numpy as np
    import pandas as pd
    import tflearn
    import tensorflow as tf
    import random
    import json
    import string
    import unicodedata
    import re
    import string
    import math

    from nltk.tokenize import TweetTokenizer
    tknzr = TweetTokenizer()

    root = getRoot(sys.argv[2])

    print("Loading dimensions")
    dimension = readDimensions(root[1])
    print("Loading categories")
    categories = loadCategories(root[2])
    print("Loading words")
    words = loadWords(root[3])

    # reset underlying graph data
    net = tflearn.input_data(shape=[None, int(dimension[0])])
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, 8)
    net = tflearn.fully_connected(net, int(dimension[1]), activation='softmax')
    net = tflearn.regression(net)
    model = tflearn.DNN(net)
    # load our saved model
    print("Loading model")
    model.load(root[0])


    # thrown = 0

    file = sys.argv[1]#"Data Row/M3-W3-Consolidado.csv"
    df = pd.read_csv(file, index_col=False, header=0, encoding='utf-8')
    

    if ('Author ID' in df.columns and 'Bio' in df.columns):


        # threshold = 0.7

        print("Predicting...")
        report = open(sys.argv[3],"w",encoding="utf-8")
        report.write("Author ID,Prediction,Value,"+','.join(categories)+"\n")

        for index, row in df.iterrows():

            user, sentence = str(row['Author ID']), str(row['Bio'])

            # user,sentence,category,value = prediction(user, sentence, categories, model, words)
            user,sentence,category,value, values = full_prediction(user, sentence, categories, model, words)
            line = ','.join([user, category, value, values]).lower()
            report.write(line+"\n")

            # if float(value) >= threshold: 
            #     line = ','.join([user, category, value, values])
            #     report.write(line+"\n")
            # else: 
            #     thrown +=1

        report.close()
        print("Results are ready")
        # print(thrown, 'of', index)
        
    else:
        print(" 'Author ID' and 'Bio' columns must be columns at %s " % (file))

else:

    print("python 3-classify_text.py <input_file_users> <input_files_models> <output_file_name>")
    print("e.g.: python 3-classify_text.py \"data_raw/M3-W3-Consolidado.csv\" \"output/files1\" \"report.csv\" ")
    print("The <input_file_users> is a comma-separated file with 'Author ID' and 'Bio' columns")
