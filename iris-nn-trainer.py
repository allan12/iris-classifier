import sys


def clean(bio):
    bio = bio.strip()
    #delete urls
    bio = re.sub(r'^https?:\/\/.*[\r\n]*', '', bio, flags=re.MULTILINE)
    bio = re.sub(r"http\S+", "", bio, flags=re.MULTILINE)
    #delete emails
    bio = re.sub(r'[\w\.-]+@[\w\.-]+',"", bio, flags=re.MULTILINE)
    #delete punctuation except mentions and users
    exclude = set(string.punctuation.replace('#','').replace('@',''))
    exclude.add('°')
    exclude.add('”')
    exclude.add('“')
    bio = ''.join(c for c in bio if c not in exclude)
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)
    bio = emoji_pattern.sub(r'', bio) # no emoji
    return bio

def loadFilesPOS(mypath):
	
	onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
	categories = [category.replace("_kws.csv","").replace("_"," ").lower() for category in onlyfiles]

	words = []
	docs = []
	for filename in onlyfiles:
		alist = [' '.join(tknzr.tokenize(clean(line.rstrip().lower()))) for line in open(mypath+'/'+filename,"r", encoding='utf-8')]
		alist = [word for word in ' '.join(alist).split(" ") if word not in stopwords.words('english')]
		#word = alist[0].split(" ")
		docs.append( (alist,filename.replace("_kws.csv","").replace("_"," ").lower()) )
		words += alist

	words = list(set(words))
	words.sort()

	return docs,categories, words

def loadFilesPreviously(mypath):
	words = []
	docs = []
	categories = []

	onlyfolders = [f for f in listdir(mypath) if join(mypath, f)]
	for folder in onlyfolders:
		onlyfiles = [f for f in listdir(mypath+"/"+folder) if isfile(join(mypath+"/"+folder, f))]
		categories.append(folder)
		for filename in onlyfiles:
			alist = [' '.join(tknzr.tokenize(clean(line.rstrip().lower()))) for line in open(mypath+"/"+folder+'/'+filename,"r", encoding='utf-8')]
			alist = [word for word in ' '.join(alist).split(" ") if word not in stopwords.words('english')]
			docs.append( (alist, folder) )
			words += alist
				
	words = list(set(words))
	words.sort()

	return docs,categories, words

def getTrainingSet(docs, categories, words):
	stemmer = LancasterStemmer()
	# create our training data
	training = []
	output = []
	# create an empty array for our output
	output_empty = [0] * len(categories)


	for doc in docs:
	    # initialize our bag of words(bow) for each document in the list
	    bow = []
	    # list of tokenized words for the pattern
	    token_words = doc[0]
	    # stem each word
	    token_words = [stemmer.stem(word.lower()) for word in token_words]
	    # create our bag of words array
	    for w in words:
	        bow.append(1) if w in token_words else bow.append(0)

	    output_row = list(output_empty)
	    output_row[categories.index(doc[1])] = 1

	    # our training set will contain a the bag of words model and the output row that tells
	    # which catefory that bow belongs to.
	    training.append([bow, output_row])

	return training

def getRoot():
	
	defaultOutput = "output"

	if not os.path.exists(defaultOutput):
		os.makedirs(defaultOutput)

	defaultFiles = "files"
	lst = [os.path.join(defaultOutput, o) for o in os.listdir(defaultOutput) if os.path.isdir(os.path.join(defaultOutput,o)) and defaultFiles in o ]

	os.makedirs(os.path.join(defaultOutput, defaultFiles+str(len(lst)+1)))

	root = os.path.join(defaultOutput, defaultFiles+str(len(lst)+1)) + "\\"

	return [ root+file for file in ['model.tflearn',"dimension.csv","categories.csv","words.csv"] ]

if len(sys.argv) == 2:

	from nltk.stem.lancaster import LancasterStemmer
	from nltk.corpus import stopwords
	import tflearn
	import tensorflow as tf
	import numpy as np

	from os import listdir
	from os.path import isfile, join
	import os
	import random
	import json
	import string
	import unicodedata

	import re
	import string

	from nltk.tokenize import TweetTokenizer
	tknzr = TweetTokenizer()

	print("Loading categories")
	docs, categories, words = loadFilesPreviously(sys.argv[1])
	print("Loaded categories: ",categories,"("+str(len(categories))+")")
	print("Loading training set")
	training = getTrainingSet(docs, categories, words)
	print("Loaded training set")

	# shuffle our features and turn into np.array as tensorflow  takes in numpy array
	random.shuffle(training)
	training = np.array(training)

	# trainX contains the Bag of words and train_y contains the label/ category
	train_x = list(training[:, 0])
	train_y = list(training[:, 1])

	print("Creating DNN")
	# reset underlying graph data
	tf.reset_default_graph()
	# Build neural network
	net = tflearn.input_data(shape=[None, len(train_x[0])])
	net = tflearn.fully_connected(net, 8)
	net = tflearn.fully_connected(net, 8)
	net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
	net = tflearn.regression(net)

	root = getRoot()

	# Define model and setup tensorboard
	model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')
	# Start training (apply gradient descent algorithm)
	model.fit(train_x, train_y, n_epoch=500, batch_size=12, show_metric=False)
	model.save(root[0])
	print("MODEL SAVED")

	dimension = open(root[1], "w", encoding="utf-8")
	dimension.write(str(len(train_x[0]))+","+str(len(train_y[0])))
	dimension.close()

	categoriesFile = open(root[2], "w", encoding="utf-8")
	categoriesFile.write('\n'.join(categories))
	categoriesFile.close()

	wordsFile = open(root[3], "w", encoding="utf-8")
	wordsFile.write('\n'.join(words))
	wordsFile.close()


	X_train, X_test, y_train, y_test = train_test_split( train_x, train_y, test_size = 0.3, random_state = 100)
else:

    print("python iris-nn-trainer.py <input_folder_source>")
    print("e.g.: python iris-nn-trainer.py experts")