import sys

fileName = 'user_with_bio.csv'
# folderName = 'experts_audited2'

def createProfile(folderName, categories, user, bio):

    ui.createFolder(folderName)

    for category in categories: 
        
        subpath = ui.joinFolder(folderName,category)
        ui.createFolder(subpath)

        subpath = ui.joinFile(subpath,user)
        ui.createFile(subpath, bio)

def getBio(user,tipo): 

    try:
        headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.'
                          '86 Safari/537.36'
        }

        url = r'https://twitter.com/'+user
        r = requests.get(url, timeout=20, headers=headers)
        soup = BeautifulSoup(r.text, 'html.parser')
        bio = soup.find('p', class_='ProfileHeaderCard-bio u-dir')
        text = bio.get_text(" ", strip=True).strip().replace('\r\n', ' ').replace('\n', ' ').replace(',', ' ')
        
        file = open(fileName,'a+', encoding='utf-8')
        file.write(','.join([user,tipo,text+'\n']))
        file.close()

        print('Added: '+ user)
    except AttributeError:
        print("AttributeError")
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        print("Timeout")
    except requests.exceptions.TooManyRedirects:
        # Tell the user their URL was bad and try a different one
        print("TooManyRedirects")
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print("RequestException")

    


if len(sys.argv) == 3:

    import requests
    from bs4 import BeautifulSoup
    import pandas as pd
    import threading
    import os
    import iris.utils as ui

    file = sys.argv[1]
    folderName = sys.argv[2]
    list_threads = []

    ui.removeFile(fileName)
    
    df = pd.read_csv(file, header=None, encoding='utf-8')

    for index, row in df.iterrows():
        
        user = row[0]
        tipo = row[1]

        t = threading.Thread(target=getBio, args=(user, tipo,))
        list_threads.append(t)
        t.start()
    
    print("All threads are started")

    for t in list_threads:
        t.join() # Wait until thread terminates its task

    print("All threads completed")

    df = pd.read_csv(fileName,names =["User","Type","Bio"], encoding='utf-8')

    values = df[df["Bio"].notnull()]["Type"].unique()

    for value in values:

      df2 = df[ (df["Bio"].notnull()) & (df["Type"]==value)]

      for index, row in df2.iterrows():

          print(row['Type'],row['User'])

          #create folder if needed
          ui.createFolder(folderName)
          ui.createFolder(folderName+'\\'+row['Type'])

          file = open(folderName+'\\'+row['Type']+'\\'+row['User']+'.txt','w', encoding='utf-8')
          file.write(str(row["Bio"])+'\n')
          file.close()

    ui.removeFile(fileName)

else:

    print("python load-bio.py <file_with_user> <experts_directory_to_save>")
    print("e.g.: python load-bio.py users-audited.csv experts_audited-N\n")
    print("file_with_user is a comma separeted file with users and type. No headers are needed")
    print("experts_directory_to_save is a directory with the previous files to add new files to the structure.")
    print("\t\t\tIt creates the directory structure if it is needed")

