import sys

fileName = 'user_with_bio-test.csv'

def getBio(user,tipos, withcategories): 

   
    try:
        
        text = ''
        url = r'https://twitter.com/'+user
        r = requests.get(url, timeout=20)
        soup = BeautifulSoup(r.text, 'html.parser')
        bio = soup.find('p', class_='ProfileHeaderCard-bio u-dir')
        text = bio.get_text(" ", strip=True).strip().replace('\r\n', ' ').replace('\n', ' ').replace(',', ' ')

        print('Added: '+ user)

        if len(text) > 0:

            file = open(fileName,'a+', encoding='utf-8')

            datos = []
            datos.append(user)
            datos.append(':'.join(tipos)) if withcategories else None
            datos.append(text+'\n')

            file.write(','.join(datos))
            file.close()


    except AttributeError:
        text = ''

if len(sys.argv) == 3:

    import requests
    from bs4 import BeautifulSoup
    import pandas as pd
    import threading
    import os
    import iris.utils as ui


    folder = sys.argv[1]
    files = ui.loadAll(folder)
    child = list(files.keys())[0]
    files = files[child]


    withcategories = sys.argv[2] == '-c'

    list_threads = []

    for file in files.keys():

        df = pd.read_csv(folder+'\\'+file, encoding='utf-8')
        
        for index, row in df.iterrows():
            
            user = row[0]
            categories = [elemento.replace('\n','') for elemento in df.columns[1:][row[1:]==1.0].values]
            
            if len(categories) > 0:
                print( user,  categories )

                t = threading.Thread(target=getBio, args=(user, categories,withcategories,))
                list_threads.append(t)
                t.start()
        
        print("All threads are started")

        for t in list_threads:
            t.join() # Wait until thread terminates its task

        print("All threads completed")

else:
    print("python load-test.py <users_with_categories> <type>")
    print("users_with_categories is a folder with comma separeted files with user and categories (ones and zeros)")
    print("type: -nc  or -c, without categories or with")