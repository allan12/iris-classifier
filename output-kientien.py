import iris.utils as ui 


test = ui.loadCSV('prediction-test/results-test.csv')
ui.saveCSV('prediction-test/results-press.csv', test[test['Prediction']=='Press']['Author ID'])
ui.saveCSV('prediction-test/results-googlers.csv', test[test['Prediction']=='Googlers']['Author ID'])
ui.saveCSV('prediction-test/results-engineers.csv', test[test['Prediction']=='Engineers']['Author ID'])