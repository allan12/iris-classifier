import requests
from bs4 import BeautifulSoup

def getUser(words): 

    try:
        headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.'
                          '86 Safari/537.36'
        }
        query = ' '.join(['"'+word+'"' for word in words.split(' ')]).replace(" "," or ")
        print(query)

        url = r'https://twitter.com/search?f=users&vertical=default&q='+query+'&src=typd&lang=en'
        r = requests.get(url, timeout=20, headers=headers)
        soup = BeautifulSoup(r.text, 'html.parser')
        users = soup.findAll('span', class_='username')
        print(list(set([user.getText() for user in users if len(user.getText()) > 1 and user.getText().startswith("@") ])))
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        print("Timeout")
    except requests.exceptions.TooManyRedirects:
        # Tell the user their URL was bad and try a different one
        print("TooManyRedirects")
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print("RequestException")


getUser("world computer")
