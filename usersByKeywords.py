import iris.utils as ui
import glob
import collections
import requests
from bs4 import BeautifulSoup


def getPermutations(query):

    permutation = []

    for idx in range(len(query)):
        pivot = query[idx]
        sub = query[idx+1:]

        for element in sub:
            permutation.append([pivot, element])

    return permutation 

def getUsers(words): 

    try:
        headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.'
                          '86 Safari/537.36'
        }
        query = ' '.join(['"'+word+'"' for word in words.split(' ')]).replace(" "," or ")
        print(query)

        url = r'https://twitter.com/search?f=users&vertical=default&q='+query+'&src=typd&lang=en'
        r = requests.get(url, timeout=20, headers=headers)
        soup = BeautifulSoup(r.text, 'html.parser')
        users = soup.findAll('span', class_='username')
        return list(set([user.getText() for user in users if len(user.getText()) > 1 and user.getText().startswith("@") ]))
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        print("Timeout")
    except requests.exceptions.TooManyRedirects:
        # Tell the user their URL was bad and try a different one
        print("TooManyRedirects")
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print("RequestException")
    return []

# categories = []

# folderRoot = 'experts'
# files = ui.loadAll(folderRoot)

# for category, values in files[folderRoot].items():
    
#     words = set()
#     categories.append(category)

#     filePath = '\\'.join([folderRoot, category, '/*.txt'])
#     for file in glob.glob(filePath):
#         with open(file, "r", encoding='utf-8') as paper:
#             # print( file, ui.clean(paper.read()) )
#             words.update( set(ui.clean(paper.read()).split(' ')) ) 

#     numbers = set( [word for word in list(words) if word.isdigit()] ) 
#     print(numbers)

#     words.discard(numbers)


#     file = open('f'+category+".txt", 'w', encoding='utf-8')
#     for word in list(words):
#         # print(word)
#         file.write(word+'\n')
#     file.close()


categories = []

folderRoot = 'experts'
files = ui.loadAll(folderRoot)

allwords =[]

categories = ['Engineers', "Googlers", "Press"]
#categories = ['Content Creators', 'Engineers', 'Entrepreneurs & Small Biz Owners', 'Executives', 'Googlers', "Gov't Officials & Policy Makers", 'Press', 'Students', 'None']


for category in categories:
    print(category)
# for category, values in files[folderRoot].items():
    
    wordcount = {}
    words = []
    # categories.append(category)

    filePath = '\\'.join([folderRoot, category, '/*.txt'])
    for file in glob.glob(filePath):
        with open(file, "r", encoding='utf-8') as paper:
            words += ui.removeStopWords(ui.clean(paper.read())).split(' ')

    numbers = set( [word for word in list(words) if word.isdigit()] )

    if category != 'None':
        allwords += words
    elif category == 'None':
        words = [word for word in words if word not in allwords ]

    for word in words:
        if word not in numbers and len(word) > 3:
            if word not in wordcount:
                wordcount[word] = 1
            else:
                wordcount[word] += 1

    

    word_counter = collections.Counter(wordcount)
    
    query = []

    for word, count in word_counter.most_common(50):
        query.append(word)

    print("Words at query: ",len(query))

    query = getPermutations(query)

    print("Total subquery: ",len(query))
    
    users = []


    for element in query:
         users += getUsers(" ".join(element))

    users = list(set(users))


    file = open('users/'+category+".txt", 'w', encoding='utf-8')
    for user in users:
        print('\t'+user)
        file.write(','.join([user,category])+'\n')
    file.close()