import glob
import iris.utils as ui
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction.text import TfidfVectorizer


def createCorpus():
    categories = []
    corpus = []

    folderRoot = 'experts'
    files = ui.loadAll(folderRoot)

    for category, values in files[folderRoot].items():
        
        categories.append(category)

        filePath = '\\'.join([folderRoot, category, '/*.txt'])
        for file in glob.glob(filePath):
            with open(file, "r", encoding='utf-8') as paper:
                corpus.append((file, ui.removeStopWords(ui.clean(paper.read()))) ) 

    return corpus, categories

def loadTest(filePath, corpus):
     with open(filePath, "r", encoding='utf-8') as paper:
        for line in paper.readlines():
            elements = line.strip().split(",")
            if len(elements) == 3:
                # print(elements[0], "->",elements[1], "->", elements[2])
                corpus.append((ui.itself+'+'+elements[1]+'+'+elements[0], ui.removeStopWords(ui.clean( elements[2]))) ) 


def createMatrix(corpus):
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df = 0, stop_words = 'english')
    tfidf_matrix =  tf.fit_transform([content for file, content in corpus])

    return tfidf_matrix

def find_similar(tfidf_matrix, index, top_n = 5):
    cosine_similarities = linear_kernel(tfidf_matrix[index:index+1], tfidf_matrix).flatten()
    related_docs_indices = [i for i in cosine_similarities.argsort()[::-1] if i != index]
    return [(index, cosine_similarities[index]) for index in related_docs_indices][0:top_n]


def testPrediction():

    indexesToPredict = [index for index in range(len(corpus)) if ui.itself in corpus[index][0]]

    predicted = []

    for idx in indexesToPredict:

        results = dict()

        prediction = []

        # print("Predicting: ",corpus[idx])
        wordsNew = set(corpus[idx][1].split(' '))

        x,labeled,user = corpus[idx][0].split("+")
        text = corpus[idx][1]

        total, nitems = 0,0

        for index, score in find_similar(tfidf_matrix, idx, top_n=10):

            if not itself in corpus[index][0]:

                wordRelated = set(corpus[index][1].split(' '))
                # print("\t",round(score,2), corpus[index][0], corpus[index][1], "\t\tMatch: " , (wordsNew & wordRelated) - {''})

                for k in categories:
                    if k in corpus[index][0]:
                        v = results.get(k, dict())
                        v['counter'] = v.get('counter',0) + 1
                        v['cumsum'] = v.get('cumsum',0) + round(score,2)

                        total += round(score,2)
                        nitems += 1

                        results[k] = v
                    
        newlabels = []
        for k,v in results.items():
            category = k
            factor = round(round(100*round(v['cumsum'],2)*round(v['counter'],2),2)/round(1+(round(total,2)*round(nitems,2))),2)
            # print(category, factor)
            newlabels.append((category, factor))

        newlabels.sort(key=lambda tup: tup[1], reverse=True)
        newlabels = [str(newlabel[0]) for newlabel in newlabels]

        prediction.append(user)
        prediction.append(labeled)
        prediction.append(':'.join(newlabels))
        prediction.append(text)
        predicted.append(prediction)


    file = open('results-totest.csv','w',encoding='utf-8')
    for pre in predicted:
        text = ','.join(pre)
        print(text)
        file.write(text+'\n')
    file.close()

def temp():
    
    indexesToPredict = [index for index in range(len(corpus)) if itself in corpus[index][0]]

    for idx in indexesToPredict:

        results = dict()

        print("Predicting: ",corpus[idx])
        wordsNew = set(corpus[idx][1].split(' '))

        total, nitems = 0,0

        for index, score in find_similar(tfidf_matrix, idx, top_n=10):

            if not itself in corpus[index][0]:

                wordRelated = set(corpus[index][1].split(' '))
                # print("\t",round(score,2), corpus[index][0], corpus[index][1], "\t\tMatch: " , (wordsNew & wordRelated) - {''})

                for k in categories:
                    if k in corpus[index][0]:
                        v = results.get(k, dict())
                        v['counter'] = v.get('counter',0) + 1
                        v['cumsum'] = v.get('cumsum',0) + round(score,2)

                        total += round(score,2)
                        nitems += 1

                        results[k] = v
                    

        for k,v in results.items():
            category = k
            factor = round(round(100*round(v['cumsum'],2)*round(v['counter'],2),2)/round(round(total,2)*round(nitems,2)),2)
            # print(k,round(v['cumsum'],2), round(v['counter'],2), round(total,2),  round(nitems,2) )
            # print(round(v['cumsum'],2),round(total,2))
            # print(round(v['counter'],2),round(nitems,2))
            print(category, factor)

    # print(tf.get_feature_names())


    # No de categorías
    # No de elementos por categoría
    # Total por categoría
    # No de elementos totales
    # Total de todos los elementos
    # No palabras por categoría

corpus, categories = createCorpus()
loadTest('user_with_bio-test.csv', corpus)

tfidf_matrix = createMatrix(corpus)

testPrediction()